# HOF Matchmaking plugin

The HOF matchmaking (HOF MM) service provides clients with a way to provide asynchronous matchmaking1 in their games. Players will be able to wager Ethereum-based tokens. By default the HOF tokens will be used, but the client can choose to use a custom token for extra costs. The client decides what the outcome of a match is either in their own custom backend using the provided HOF MM SDK or using the backend managed by HOF (with extra costs.)

This SDK provides some simple tools to communicate with the HOF MM backend and to easily implement user login.

## Getting started

The plugin is built and tested in Unity 2020.3+. It may work in older versions but there's no guarantee.

There's essentially 2 ways to integrate the plugin into your Unity project: via the Unity package manager and directly via a unitypackage.

### Unity package manager

The plugin can be added to the packagemanager with the following url:  
`https://gitlab.com/hofenterprise/hof-mm-plugin.git?path=Package`

![UPM add git repo](https://hof-ent.com/assets/uploads/mm/upm-add-git.png)

![UPM add git repo 2](https://hof-ent.com/assets/uploads/mm/upm-add-git-2.png)

Alternatively you can manually add `"com.hofent.matchmaking": "https://gitlab.com/hofenterprise/hof-mm-plugin.git?path=Package"` to the `Packages/manifest.json` file.

The plugin is versioned as `v{major}.{minor}.{patch}` (e.g. `v1.0.0`). So if you need to include a specific version of the plugin in your project, you can use the url: `https://gitlab.com/hofenterprise/hof-mm-plugin.git?path=Package#v1.0.0`.

### Unity package

To add the plugin directly, you can download the unity package uploaded with each release from [Gitlab](https://gitlab.com/hofenterprise/hof-mm-plugin/-/releases). Then simply drag the `.unitypackage` file into your project to unpack it.


## Features

Most of the HOF MM service is handled in the back-end, so the plugin is fairly simple. There's 2 main features:
 * A utility for making API requests to the HOF MM back-end that don't require authorization using the app's private key.
 * A utility for showing a native webview on Android and iOS and an example implementation using the utility.

### HOF MM API

The API endpoints can be accessed via the `HofEnt.Matchmaking.API` class. This class contains 2 methods:

#### GetUser

This method fetches some basic information about a user identified by their access token. This does not require the private key of the app, as the access token is already linked to one app.

```csharp
var user = await API.GetUser("[User access token]");

Debug.Log($"Welcome {user.Fullname}!");
```

#### GetUserBalance

This method fetches the current HOF token balance of the user identified by the given access token. This does not include tokens that are currently in escrow (i.e. wagered on active matches in any app).

```csharp
var balance = await API.GetUserBalance("[User access token]");

Debug.Log($"Current balance: {balance}");
```

#### Asynchronous calls

There's 3 ways to await API calls: Using coroutines and callbacks, using C# tasks or using [UniTask](https://github.com/Cysharp/UniTask). The recommended way is to use the UniTask library as it provides an allocation free async/await integration.

If UniTask is added to the Unity package manager, then the `UNITASK` symbol is automatically specified. This will switch the API to use the UniTask library for awaiting API calls.

Alternatively you may specify the `TASK` symbol to use the regular async/await tasks. UniTask will always take priority, so make sure it is not in the project if you specifically want to use normal tasks.

Lastly if neither is specified, each API method has an additional callback parameter that is called when the API call returns.

```csharp
// UniTask
var unitaskUsr = await API.GetUser("[User access token]");

// Task
var taskUsr = await API.GetUser("[User access token]");

// Callback
API.GetUser("[User access token]", usr => {
    // Do something with usr
});

```

### Webview

To do anything with a user, an access token needs to be obtained first. This is done via the OAuth protocol.  
One way to obtain an access token is by opening a browser to the OAuth login url and providing a redirect URI that the app is listening to. An alternative an a more graceful way is by opening a webview on top of your app. This plugin contains an implementation of a native webiew for Android and iOS (using the [unity-webview](https://github.com/gree/unity-webview) library by gree.).

To use the webview, simply take the `Webview` prefab from the `Examples/Prefabs` folder and place it in your scene. This already contains a simple UI around the webview. You can also create your own webview manager by using the `WebviewAdapter` class to show and hide the native webview and catch events.

A public API key will also have to be specified. It is used to determine which app the user will authorize. This can be done by generating a HOF settings file from the `HOF-MM > Open settings` menu in the top toolbar. When selected a `HOFMMSettings.asset` file will be created in the `Assets/Resources` folder. This file can be moved to anywhere within the assets folder if necessary, so long as it is always in the root of a `Resources` folder. Select the file to show its options in the inpsector. It is here that the public API key should be specified. See the [documentation](https://dev.mm.hof-ent.com/docs/HMM/Wiki/app-integration.html#generating-public--private-keys) for more info on how to generate the public API key.

![Plugin settings menu](https://hof-ent.com/assets/uploads/mm/plugin-settings.png)

<a href="match-api.md">&laquo; Managing matches</a>