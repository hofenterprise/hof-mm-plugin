﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITASK
using Cysharp.Threading.Tasks;

#elif TASK
using System.Threading.Tasks;
#else
using System.Collections;
#endif

namespace HofEnt.Matchmaking
{
    /// <summary>
    /// Webview for authenticating this app to a user's HOF MM account
    /// </summary>
    public class LoginWebView : MonoBehaviour
    {
        #region Events

        /// <summary>
        /// Returns the given parameters from the backend or null if the operation was cancelled
        /// </summary>
        public event Action<Dictionary<string, string>> OnResult;

        #endregion

        #region Unity editor fields

        [SerializeField] private WebviewAdapter _adapter;
        [SerializeField] private Button _backBtn;
        [SerializeField] private Button _forwardBtn;
        [SerializeField] private Button _refreshBtn;
        [SerializeField] private Button _closeBtn;

        [Header("UI"), SerializeField] private Canvas _canvas;
        [SerializeField] private float _showTime = 0.3f;
        [SerializeField] private RectTransform _uiView;
        [SerializeField] private AnimationCurve _animationCurve;

        #endregion

        #region Fields

        private HOFMMSettings _settings;
        private float _screenHeight;

        #endregion

        #region Setup

        private void Reset()
        {
            _canvas = GetComponentInParent<Canvas>();
            if (_canvas == null)
                _canvas = FindObjectOfType<Canvas>();
        }

        private void Awake()
        {
            _backBtn.onClick.AddListener(BackBtn_OnClick);
            _forwardBtn.onClick.AddListener(ForwardBtn_OnClick);
            _refreshBtn.onClick.AddListener(RefreshBtn_OnClick);
            _closeBtn.onClick.AddListener(CloseBtn_OnClick);

            _adapter.OnStarted += WebviewAdapter_OnStarted;
            _adapter.OnLoaded += WebviewAdapter_OnLoaded;
            _adapter.OnHooked += WebviewAdapter_OnHooked;
        }

        private void OnDestroy()
        {
            _backBtn.onClick.RemoveListener(BackBtn_OnClick);
            _forwardBtn.onClick.RemoveListener(ForwardBtn_OnClick);
            _refreshBtn.onClick.RemoveListener(RefreshBtn_OnClick);
            _closeBtn.onClick.RemoveListener(CloseBtn_OnClick);

            _adapter.OnStarted -= WebviewAdapter_OnStarted;
            _adapter.OnLoaded -= WebviewAdapter_OnLoaded;
            _adapter.OnHooked -= WebviewAdapter_OnHooked;
        }

        private void Start()
        {
            _screenHeight = Screen.height;

            _uiView.anchoredPosition = new Vector2(0, -Screen.height / _canvas.scaleFactor);
            _adapter.SetMargins(0, Mathf.RoundToInt(100 * _canvas.scaleFactor), 0, 0);
        }

        #endregion

        #region Update

        private void LateUpdate()
        {
            if (Screen.height == _screenHeight)
                return;

            // Update ui position when screen size changes
            _screenHeight = Screen.height;
            _uiView.anchoredPosition = Vector2.up * (-Screen.height / _canvas.scaleFactor);
        }

        #endregion

        #region EventHandlers

        private void BackBtn_OnClick()
        {
            _adapter.Back();
        }

        private void ForwardBtn_OnClick()
        {
            _adapter.Forward();
        }

        private void RefreshBtn_OnClick()
        {
            _adapter.Refresh();
        }

        private void CloseBtn_OnClick()
        {
            OnResult?.Invoke(null);

#if UNITASK
            Hide().Forget();
#elif TASK
            Hide();
#else
            StartCoroutine(Hide());
#endif
        }

        /// <summary>
        /// Webview was opened with given url
        /// </summary>
        /// <param name="url">Url that the webview was opened with</param>
        private void WebviewAdapter_OnStarted(string url)
        {
            _backBtn.interactable = false;
            _forwardBtn.interactable = false;
            _refreshBtn.interactable = false;
        }

        /// <summary>
        /// The webview has loaded a page.
        /// </summary>
        /// <param name="url">The url of the page the webview has loaded</param>
        private void WebviewAdapter_OnLoaded(string url)
        {
            _backBtn.interactable = _adapter.CanGoBack;
            _forwardBtn.interactable = _adapter.CanGoForward;
            _refreshBtn.interactable = true;
        }

        /// <summary>
        /// The webview received a hook (deeplink), meaning it got a result from the OAuth service.
        /// </summary>
        /// <param name="url"></param>
        private void WebviewAdapter_OnHooked(string url)
        {
            // Extract variables from url
            var data = url.Substring(9);
            var res = new Dictionary<string, string>();
            var vars = data.Split('&');
            foreach (var var in vars)
            {
                var parts = var.Split('=');
                var val = parts.Length >= 2 ? parts[1] : null;
                res.Add(parts[0], val);
            }

            // Broadcast result
            OnResult?.Invoke(res);

#if UNITASK
            Hide().Forget();
#elif TASK
            Hide();
#else
            StartCoroutine(Hide());
#endif
        }

        #endregion

        #region Public

        /// <summary>
        /// Shows the webview and loads the landing page for the backend login page
        /// </summary>
#if UNITASK
        public async UniTask Show()
#elif TASK
        public async Task Show()
#else
        public IEnumerator Show()
#endif
        {
            if (_settings == null)
                _settings = Resources.Load<HOFMMSettings>("HOFMMSettings");

            // Show the UI based on set curve
            var timePassed = 0f;
            while (timePassed <= _showTime)
            {
#if UNITASK
                await UniTask.Yield();
#elif TASK
                await Task.Yield();
#else
                yield return null;
#endif

                timePassed += Time.deltaTime;
                var perc = _animationCurve.Evaluate(Mathf.Clamp01(timePassed / _showTime));
                _uiView.anchoredPosition =
                    Vector2.up * ((Screen.height - Screen.height * perc) / -_canvas.scaleFactor);
            }

            _uiView.anchoredPosition = Vector2.zero;

            _adapter.Visible = true;

            // Actually load the URL in the webview and await the result
            var url =
                $"https://hofmmdev.picorose.com/api/oauth?client_id={_settings.PublicApiKey}&packagename={Application.identifier}";
#if UNITASK
            await _adapter.LoadUrl(url);
#elif TASK
            await _adapter.LoadUrl(url);
#else
            yield return _adapter.LoadUrl(url);
#endif
        }

        #endregion

        #region Private

#if UNITASK
        private async UniTask Hide()
#elif TASK
        private async Task Hide()
#else
        private IEnumerator Hide()
#endif
        {
            _adapter.Visible = false;

            // Play the hide animation
            var timePassed = 0f;
            while (timePassed <= _showTime)
            {
#if UNITASK
                await UniTask.Yield();
#elif TASK
                await Task.Yield();
#else
                yield return null;
#endif

                timePassed += Time.deltaTime;
                var perc = _animationCurve.Evaluate(1 - Mathf.Clamp01(timePassed / _showTime));
                _uiView.anchoredPosition =
                    Vector2.up * ((Screen.height - Screen.height * perc) / -_canvas.scaleFactor);
            }

            _uiView.anchoredPosition = Vector2.up * (-Screen.height / _canvas.scaleFactor);
        }

        #endregion
    }
}