﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
#if UNITASK
using Cysharp.Threading.Tasks;

#elif TASK
using System.Threading.Tasks;

#else
using System.Collections;
#endif

namespace HofEnt.Matchmaking
{
    /// <summary>
    /// Adapter for interacting with the webview plugin
    /// </summary>
    [RequireComponent(typeof(WebViewObject))]
    public class WebviewAdapter : MonoBehaviour
    {
        #region Events

        /// <summary>
        /// Called when a piece of javascript code is executed
        /// </summary>
        public event Action<string> OnJavascript;

        /// <summary>
        /// Called when an error is logged
        /// </summary>
        public event Action<string> OnError;

        /// <summary>
        /// Called when webview is initalized
        /// </summary>
        public event Action<string> OnStarted;

        /// <summary>
        /// Called when hook link is caught
        /// </summary>
        public event Action<string> OnHooked;

        /// <summary>
        /// Called when a webpage is loaded
        /// </summary>
        public event Action<string> OnLoaded;

        #endregion

        #region Unity editor fields

        [SerializeField] private WebViewObject _webViewObject;

        #endregion

        #region Fields

        private bool _visible;
        private bool _isLoading;

        #endregion

        #region Properties

        /// <summary>
        /// True if there's a page before the current page in the history
        /// </summary>
        public bool CanGoBack => _webViewObject.CanGoBack();

        /// <summary>
        /// True if there's a page after the current page in the history
        /// </summary>
        public bool CanGoForward => _webViewObject.CanGoForward();

        /// <summary>
        /// Percentage the page is loaded
        /// </summary>
        public int Progress => _webViewObject.Progress();

        /// <summary>
        /// Visibility of the webview
        /// </summary>
        public bool Visible
        {
            get => _visible;
            set
            {
                _visible = value;
                if (value)
                {
                    if (!_isLoading)
                        _webViewObject.SetVisibility(true);
                }
                else
                    _webViewObject.SetVisibility(false);
            }
        }

        #endregion

        #region Setup

        private void Reset()
        {
            _webViewObject = GetComponent<WebViewObject>();
        }

        private void Awake()
        {
            _webViewObject.Init(
                HandleJS,
                err: HandleError,
                started: HandleStarted,
                hooked: HandleHooked,
                ld: HandleLoaded,
                transparent: false,
                zoom: true,
#if UNITY_EDITOR
                separated: false,
#endif
                enableWKWebView: true,
                wkContentMode: 0);

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        _webviewObject.bitmapRefreshCycle = 1;
#endif

            _webViewObject.SetURLPattern("https://dev.mm.hof-ent.com/api/*", "", "^hofmm://#.*");
            _webViewObject.SetTextZoom(100);
            _webViewObject.SetVisibility(false);
        }

        #endregion

        #region EventHandlers

        private void HandleJS(string msg)
        {
            OnJavascript?.Invoke(msg);
        }

        private void HandleError(string msg)
        {
            OnError?.Invoke(msg);
        }

        private void HandleStarted(string msg)
        {
            // Hide webview while it's loading
            _isLoading = true;
            _webViewObject.SetVisibility(false);
            OnStarted?.Invoke(msg);
        }

        private void HandleHooked(string msg)
        {
            OnHooked?.Invoke(msg);
        }

        private void HandleLoaded(string msg)
        {
#if UNITY_EDITOR_OSX || (!UNITY_ANDROID && !UNITY_WEBPLAYER && !UNITY_WEBGL)
            // NOTE: depending on the situation, you might prefer
            // the 'iframe' approach.
            // cf. https://github.com/gree/unity-webview/issues/189
#if true
            _webViewObject.EvaluateJS(@"
                  if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
                    window.Unity = {
                      call: function(msg) {
                        window.webkit.messageHandlers.unityControl.postMessage(msg);
                      }
                    }
                  } else {
                    window.Unity = {
                      call: function(msg) {
                        window.location = 'unity:' + msg;
                      }
                    }
                  }
                ");
#else
                _webViewObject.EvaluateJS(@"
                  if (window && window.webkit && window.webkit.messageHandlers && window.webkit.messageHandlers.unityControl) {
                    window.Unity = {
                      call: function(msg) {
                        window.webkit.messageHandlers.unityControl.postMessage(msg);
                      }
                    }
                  } else {
                    window.Unity = {
                      call: function(msg) {
                        var iframe = document.createElement('IFRAME');
                        iframe.setAttribute('src', 'unity:' + msg);
                        document.documentElement.appendChild(iframe);
                        iframe.parentNode.removeChild(iframe);
                        iframe = null;
                      }
                    }
                  }
                ");
#endif
#elif UNITY_WEBPLAYER || UNITY_WEBGL
                _webViewObject.EvaluateJS(
                    "window.Unity = {" +
                    "   call:function(msg) {" +
                    "       parent.unityWebView.sendMessage('WebViewObject', msg)" +
                    "   }" +
                    "};");
#endif
            _webViewObject.EvaluateJS(@"Unity.call('ua=' + navigator.userAgent)");

            OnLoaded?.Invoke(msg);
            _isLoading = false;

            if (_visible)
                _webViewObject.SetVisibility(true);
        }

        #endregion

        #region Public

        /// <summary>
        /// Loads a page in the webview with given URL
        /// </summary>
        /// <param name="url">The URL to load</param>
#if UNITASK
        public async UniTask LoadUrl(string url)
#elif TASK
        public async Task LoadUrl(string url)
#else
        public IEnumerator LoadUrl(string url)
#endif
        {
            // Hide webview while loading
            _webViewObject.SetVisibility(false);

#if !UNITY_WEBPLAYER && !UNITY_WEBGL
            if (url.StartsWith("http"))
                _webViewObject.LoadURL(url.Replace(" ", "%20"));
            else
            {
                var exts = new[]
                {
                    ".jpg",
                    ".js",
                    ".html" // should be last
                };
                foreach (var ext in exts)
                {
                    var urlEx = url.Replace(".html", ext);
                    var src = Path.Combine(Application.streamingAssetsPath, urlEx);
                    var dst = Path.Combine(Application.persistentDataPath, urlEx);
                    byte[] result;
                    if (src.Contains("://"))
                    {
                        // For Android
#if UNITY_2018_4_OR_NEWER
                        var unityWebRequest = UnityWebRequest.Get(src);
#if UNITASK
                        var req = unityWebRequest.SendWebRequest();
                        while (!req.isDone)
                            await UniTask.Yield();
#elif TASK
                        var req = unityWebRequest.SendWebRequest();
                        while (!req.isDone)
                            await Task.Yield();
#else
                        yield return unityWebRequest.SendWebRequest();
#endif
                        result = unityWebRequest.downloadHandler.data;
#else
                    var www = new WWW(src);
                    yield return www;
                    result = www.bytes;
#endif
                    }
                    else
                        result = File.ReadAllBytes(src);

                    File.WriteAllBytes(dst, result);
                    if (ext != ".html")
                        continue;

                    _webViewObject.LoadURL("file://" + dst.Replace(" ", "%20"));
                    break;
                }
            }
#else
        if (url.StartsWith("http"))
            _webViewObject.LoadURL(url.Replace(" ", "%20"));
        else
            _webViewObject.LoadURL("StreamingAssets/" + url.Replace(" ", "%20"));
#endif
        }

        /// <summary>
        /// Go back to the previous page if possible
        /// </summary>
        public void Back()
        {
            if (!CanGoBack)
                return;

            _webViewObject.GoBack();
        }

        /// <summary>
        /// Forward to the next page if possible
        /// </summary>
        public void Forward()
        {
            if (!CanGoForward)
                return;

            _webViewObject.GoForward();
        }

        /// <summary>
        /// Refresh the page
        /// </summary>
        public void Refresh()
        {
            _webViewObject.Reload();
        }

        /// <summary>
        /// Set the margins of the webview overlaid on the screen 
        /// </summary>
        /// <param name="left">Left offset</param>
        /// <param name="top">Top offset</param>
        /// <param name="right">Right offset</param>
        /// <param name="bottom">Bottom offset</param>
        public void SetMargins(int left, int top, int right, int bottom)
        {
            _webViewObject.SetMargins(left, top, right, bottom);
        }

        #endregion
    }
}