﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
#if UNITASK
using Cysharp.Threading.Tasks;

#elif TASK
using System.Threading.Tasks;
#else
using System;
using System.Collections;
#endif

namespace HofEnt.Matchmaking
{
    /// <summary>
    /// Utility class for accessing all public API endpoints in the HOF MM backend
    /// </summary>
    public static class API
    {
        #region Fields

        private const string BaseUrl = "https://hofmmdev.picorose.com/api/";

        private struct Request
        {
            #region Properties

            public string Token { get; set; }
            public string Endpoint { get; set; }
            public RequestMethod Method { get; set; }
            public Dictionary<string, object> Parameters { get; set; }

            #endregion

            #region Setup

            public Request(string token, string endpoint, RequestMethod method = RequestMethod.Get,
                Dictionary<string, object> parameters = null)
            {
                Token = token;
                Endpoint = endpoint;
                Method = method;
                Parameters = parameters;
            }

            public Request(string endpoint, RequestMethod method = RequestMethod.Get,
                Dictionary<string, object> parameters = null)
            {
                Token = null;
                Endpoint = endpoint;
                Method = method;
                Parameters = parameters;
            }

            #endregion
        }

        private enum RequestMethod
        {
            Get,
            Put,
            Post,
            Delete
        }

        #endregion

        #region Public

        /// <summary>
        /// Fetches User instance for given user authorization
        /// </summary>
        /// <param name="token">The user authorization (access token)</param>
        /// <returns>User instance for given user authorization or invalid user object when authorization is invalid</returns>
#if UNITASK
        public static UniTask<User> GetUser(string token)
#elif TASK
        public async Task<User> GetUser(string token)
#else
        public void GetUser(string token, Action<User> callback)
#endif
        {
            var req = new Request(token, "user");
#if UNITASK || TASK
            return Execute<User>(req);
#else
            Execute(req, callback)
#endif
        }

        /// <summary>
        /// Fetches HOF token balance of given user, not including the HOF tokens in escrow.
        /// </summary>
        /// <param name="token">The user authorization (access token)</param>
        /// <returns>The HOF token balance of given user, not including the HOF tokens in escrow.</returns>
#if UNITASK
        public static UniTask<float> GetUserBalance(string token)
#elif TASK
        public async Task<float> GetUserBalance(string token)
#else
        public void GetUserBalance(string token, Action<float> callback)
#endif
        {
            var req = new Request(token, "user/balance");
#if UNITASK || TASK
            return Execute<float>(req);
#else
            Execute(req, callback)
#endif
        }

        #endregion

        #region Private

        /// <summary>
        /// Takes a request structure and constructs and executes a web request to the API from it.
        /// </summary>
        /// <param name="req">The request to execute</param>
        /// <typeparam name="T">The type to return for a successful request</typeparam>
        /// <returns>Null when an error occurs or the <see cref="T"/> type instance when the request executed successfully</returns>
#if UNITASK
        private static async UniTask<T> Execute<T>(Request req)
#elif TASK
        private async Task<T> Execute<T>(Request req)
#else
        private void Execute<T>(Request req, Action<T> callback)
#endif
        {
            UnityWebRequest uwr;
            switch (req.Method)
            {
                case RequestMethod.Get:
                {
                    // Parse parameters into a query string
                    var pars = "";
                    if (req.Parameters != null)
                        foreach (var par in req.Parameters)
                        {
                            if (pars != "")
                                pars += "&";
                            pars += par.Key + "=" + par.Value;
                        }

                    // Construct web request
                    uwr = UnityWebRequest.Get($"{BaseUrl}{req.Endpoint}?{pars}");
                }
                    break;
                case RequestMethod.Delete:
                {
                    // Parse parameters into a query string
                    var pars = "";
                    if (req.Parameters != null)
                        foreach (var par in req.Parameters)
                        {
                            if (pars != "")
                                pars += "&";
                            pars += par.Key + "=" + par.Value;
                        }

                    // Construct web request
                    uwr = UnityWebRequest.Delete($"{BaseUrl}{req.Endpoint}?{pars}");
                }
                    break;
                case RequestMethod.Put:
                {
                    // Serialize parameters to json and construct web request
                    var pars = "";
                    if (req.Parameters != null)
                        pars = JsonConvert.SerializeObject(req.Parameters);
                    uwr = UnityWebRequest.Put($"{BaseUrl}{req.Endpoint}", pars);
                }
                    break;
                case RequestMethod.Post:
                {
                    // Serialize parameters to json and construct web request
                    var pars = "";
                    if (req.Parameters != null)
                        pars = JsonConvert.SerializeObject(req.Parameters);

                    uwr = UnityWebRequest.Post($"{BaseUrl}{req.Endpoint}", pars);
                }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // Set authorization header if token was given
            if (!string.IsNullOrWhiteSpace(req.Token))
                uwr.SetRequestHeader("Authorization", "Bearer " + req.Token);

#if UNITASK
            // Execute web request and wait until it's done
            var op = uwr.SendWebRequest();
            while (!op.isDone)
                await UniTask.Yield();

            // For unsuccessful requests (500 errors), log the request and result and return default value of type
            if (uwr.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(
                    $"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: {uwr.error}\n{uwr.downloadHandler.text}");
                return default;
            }

            try
            {
                // Check if message returned successful
                var msg = JsonConvert.DeserializeObject<Message>(uwr.downloadHandler.text);
                if (!msg.Success)
                {
                    Debug.LogError($"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: {msg.Data}");
                    return default;
                }

                // Try to deserialize message data into given type and return it
                var obj = JsonConvert.DeserializeObject<T>(msg.Data);
                return obj;
            }
            catch (JsonException ex)
            {
                // Json serialization failed either while trying to deserialize the message object or while
                // deserializing the message data into the given type
                Debug.LogError(ex);
                Debug.LogError(uwr.downloadHandler.text);
                Debug.LogError($"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: Json exception");
                return default;
            }
#elif TASK
            // Execute web request and wait until it's done
            var op = uwr.SendWebRequest();
            while (!op.isDone)
                await Task.Yield();
            
            // For unsuccessful requests (500 errors), log the request and result and return default value of type
            if (uwr.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError(
                    $"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: {uwr.error}\n{uwr.downloadHandler.text}");
                return default;
            }

            try
            {
                // Check if message returned successful
                var msg = JsonConvert.DeserializeObject<Message>(uwr.downloadHandler.text);
                if (!msg.Success)
                {
                    Debug.LogError($"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: {msg.Data}");
                    return default;
                }

                // Try to deserialize message data into given type and return it
                var obj = JsonConvert.DeserializeObject<T>(msg.Data);
                return obj;
            }
            catch (JsonException ex)
            {
                // Json serialization failed either while trying to deserialize the message object or while
                // deserializing the message data into the given type
                Debug.LogError(ex);
                Debug.LogError(uwr.downloadHandler.text);
                Debug.LogError($"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: Json exception");
                return default;
            }
#else
            StartCoroutine(AwaitRequest());

            IEnumerator AwaitRequest()
            {
                // Execute web request and wait until it's done
                yield return uwr.SendWebRequest();

                // For unsuccessful requests (500 errors), log the request and result and return default value of type
                if (uwr.result != UnityWebRequest.Result.Success)
                {
                    Debug.LogError($"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: {uwr.error}\n{uwr.downloadHandler.text}");
                    callback?.Invoke(default);
                } else
                {
                    try
                    {
                        // Check if message returned successful
                        var msg = JsonConvert.DeserializeObject<Message>(uwr.downloadHandler.text);
                        if (!msg.Success)
                        {
                            Debug.LogError($"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: {msg.Data}");
                            callback?.Invoke(default);
                        } else
                        {
                            // Try to deserialize message data into given type and return it
                            var obj = JsonConvert.DeserializeObject<T>(msg.Data);
                            callback?.Invoke(obj);
                        }
                    }
                    catch (JsonException ex)
                    {
                        // Json serialization failed either while trying to deserialize the message object or while
                        // deserializing the message data into the given type
                        Debug.LogError(ex);
                        Debug.LogError(uwr.downloadHandler.text);
                        Debug.LogError($"API error\nEndpoint: {req.Endpoint}\nMethod: {req.Method}\nError: Json exception");
                        callback?.Invoke(default);
                    }
                }
            }
#endif
        }

        #endregion
    }
}