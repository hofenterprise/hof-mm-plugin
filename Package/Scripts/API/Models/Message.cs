﻿using Newtonsoft.Json;

namespace HofEnt.Matchmaking
{
    /// <summary>
    /// Object representing a message received from the API
    /// </summary>
    public class Message
    {
        #region Fields

        [JsonProperty] private bool success;
        [JsonProperty] private object error;
        [JsonProperty] private object data;

        #endregion

        #region Properties

        /// <summary>
        /// False if there was some kind of error executing the request
        /// </summary>
        [JsonIgnore]
        public bool Success => success;

        /// <summary>
        /// The data of the message or error in case the request failed
        /// </summary>
        [JsonIgnore]
        public string Data => (success ? data : error)?.ToString();

        #endregion
    }
}