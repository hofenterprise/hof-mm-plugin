﻿using Newtonsoft.Json;

namespace HofEnt.Matchmaking
{
    /// <summary>
    /// Object representing a HOF MM user, contains some data the user has authorized the app to read.
    /// </summary>
    public struct User
    {
        #region Fields

        [JsonProperty] private int id;
        [JsonProperty] private string email;
        [JsonProperty] private string firstname;
        [JsonProperty] private string lastname;

        #endregion

        #region Properties

        /// <summary>
        /// Evaluates if the object is valid. The default instance of User will be invalid. This can be used to check if
        /// the instance is 'null'.
        /// </summary>
        [JsonIgnore]
        public bool IsValid => id != 0 && email != null && firstname != null && lastname != null;

        /// <summary>
        /// The unique ID of the user
        /// </summary>
        [JsonIgnore]
        public int Id => id;

        /// <summary>
        /// The email address of the user
        /// </summary>
        [JsonIgnore]
        public string Email => email;

        /// <summary>
        /// The firstname of the user
        /// </summary>
        [JsonIgnore]
        public string Firstname => firstname;

        /// <summary>
        /// The lastname of the user
        /// </summary>
        [JsonIgnore]
        public string Lastname => lastname;

        /// <summary>
        /// The firstname and lastname of the user concatenated to a single string
        /// </summary>
        [JsonIgnore]
        public string Fullname => firstname + (string.IsNullOrWhiteSpace(lastname) ? "" : " " + lastname);

        #endregion

        #region Public

        public static bool operator ==(User usr, object obj)
        {
            if (obj == null && !usr.IsValid)
                return true;

            if (!(obj is User usr2))
                return false;

            return usr.id == usr2.id && usr.email == usr2.email && usr.firstname == usr2.firstname &&
                   usr.lastname == usr2.lastname;
        }

        public static bool operator !=(User usr, object obj)
        {
            return !(usr == obj);
        }

        #endregion
    }
}