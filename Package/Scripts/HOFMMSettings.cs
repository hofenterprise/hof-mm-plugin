using System.IO;
using UnityEngine;

namespace HofEnt.Matchmaking
{
    /// <summary>
    /// Scriptable object containing HOF MM settings for this app
    /// </summary>
    public class HOFMMSettings : ScriptableObject
    {
        #region Unity editor fields

        [SerializeField] private string _publicApiKey;

        #endregion

        #region Properties

        /// <summary>
        /// The public API key used for OAuth authentication
        /// </summary>
        public string PublicApiKey => _publicApiKey;

        #endregion

        #region Private

#if UNITY_EDITOR
        [UnityEditor.MenuItem("HOF-MM/Open settings")]
        private static void LocateOrCreate()
        {
            // Try to load HOFMMSettings object from resources
            var settings = Resources.Load<HOFMMSettings>("HOFMMSettings");
            if (settings == null)
            {
                // No instance was found, create a new one
                settings = CreateInstance<HOFMMSettings>();

                // Save the object to a Resources folder in the root Assets folder
                if (!Directory.Exists(Application.dataPath + "/Resources"))
                    Directory.CreateDirectory(Application.dataPath + "/Resources");

                // Save asset to database
                UnityEditor.AssetDatabase.Refresh();
                UnityEditor.AssetDatabase.CreateAsset(settings, "Assets/Resources/HOFMMSettings.asset");
                UnityEditor.AssetDatabase.SaveAssets();
            }

            // Select settings object
            UnityEditor.Selection.activeObject = settings;
        }
#endif

        #endregion
    }
}